package com.holiash.servlets;

import com.holiash.model.Pizza;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/pizza-store/order/*")
public class OrderServlet extends HttpServlet {

  private static List<Pizza> order;

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {
    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();
    out.println("<html><body>");
    out.println("<b><a href='order'>REFRESH</a></b>");
    out.println("<h1><a href=\"../pizza-store\">Pizza Store</a><h1>");
    out.println("<h2>Your order</h2>");
    if (order != null && order.size() > 0) {
      double sum = 0.;
      out.println("<p>Pizza Name    Pizza Price</p>");
      for (Pizza p : order) {
        out.println("<p>" + p.getName() + "   " + p.getPrice() + "</p>");
        sum += p.getPrice();
      }
      out.println("<h3>Total price: " + sum + "</p>");
      out.println("<form>");
      out.println("<p><b>Remove pizza from list</b></p>");
      out.println("<p>\n<input type=\"text\" name=\"pizza_name\">");
      out.println(
        "<input type=\"button\" onclick=\"remove(this.form.pizza_name.value)\" value='remove' name='remove_btn'>\n</p>");
      out.println("</form>");
    } else {
      out.println("<h3>Your basket is empty<h3>");
      out.println("<p>get back and make your order<p>");
    }
    out.println("<script type='text/javascript'>\n" +
      "function remove(name){fetch('order/' + name, {method: 'DELETE'}); }\n" +
      "</script>");
    out.println("</body></html>");
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {
    String[] values = req.getParameterValues("pizza");
    parseValues(values);
    doGet(req, resp);
  }

  @Override
  protected void doPut(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {
    super.doPut(req, resp);
  }

  @Override
  protected void doDelete(HttpServletRequest req, HttpServletResponse resp) {
    String name = req.getRequestURI().replace("/servlet-holiash/pizza-store/order/", "");
    order.removeIf(pizza -> pizza.getName().equalsIgnoreCase(name));
  }

  private static void parseValues(String[] values) {
    order = new ArrayList<>();
    if (values != null) {
      for (String value : values) {
        String[] parsed = value.split(", ");
        order.add(new Pizza(parsed[0], Double.parseDouble(parsed[1])));
      }
    }
  }
}
