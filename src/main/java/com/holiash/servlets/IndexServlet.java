package com.holiash.servlets;

import com.holiash.model.Pizza;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebServlet("/pizza-store")
public class IndexServlet extends HttpServlet {

  private static final Logger logger = LogManager.getLogger(IndexServlet.class);
  private static Set<Pizza> availablePizzas;
  private static String[] names;

  @Override
  public void init() throws ServletException {
    logger.info(this.getServletName() + " into  init");
    availablePizzas = new HashSet<>();
    availablePizzas.add(new Pizza("carbonara", 100));
    availablePizzas.add(new Pizza("diavolo", 150));
    availablePizzas.add(new Pizza("karne", 130));
    availablePizzas.add(new Pizza("hawaian", 95));
    availablePizzas.add(new Pizza("quatro formaggi", 120));
    availablePizzas.add(new Pizza("quatro stagioni", 110));
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {
    logger.info(this.getServletName() + " into doGet()");
    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();
    out.write("<html><body>");
    out.write("<h1><a href=\"pizza-store\">Pizza Store</a><h1>");
    out.write("<form method=\"POST\" action=\"pizza-store/order\">");
    out.write("<fieldset>");
    out.write("<legend>Make your choice</legend>");
    for (Pizza p : availablePizzas) {
      String value = p.getName() + ", " + p.getPrice();
      out.write(
        "<input type=\"checkbox\" name=\"pizza\" value=\"" + value + "\">"
          + value + "UAH"
          + "<br>");
    }
    out.write("<input type = \"submit\" value = \"Submit\"/>");
    out.write("</fieldset></form>");
    out.write("</body></html>");
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {
  }

  @Override
  protected void doPut(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {
    super.doPut(req, resp);
  }

  @Override
  protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {
    super.doDelete(req, resp);
  }

  @Override
  public void destroy() {
    super.destroy();
  }
}
